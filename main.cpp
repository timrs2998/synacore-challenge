#include <stack>
#include <string>
#include <iostream>
#include <cmath>
#include <fstream>
#include <bits/unique_ptr.h>

using namespace std;

const size_t MAX_ADDRESS = pow(2, 15);
const size_t NUM_REGISTERS = 8;
const uint16_t MAX_VALUE = (uint16_t) MAX_ADDRESS;

auto initializeMemoryFromFile(const string& filename) {
    unique_ptr<uint16_t[]> memory(new uint16_t[MAX_ADDRESS + NUM_REGISTERS]);
    ifstream inFile(filename, ios::in|ios::binary);
    uint16_t value = 0;
    for (size_t pointer = 0; inFile.read((char*)&value, sizeof(uint16_t)); pointer++) {
        memory[pointer] = value;
    }
    return memory;
}

inline uint16_t getValue(const uint16_t registers[], const uint16_t& value) {
   return value < 32768 ? value : registers[value - 32768];
}

void execute(uint16_t memory[], uint16_t registers[]) {
    string inputBuffer;
    size_t pointer = 0;
    stack<uint16_t> unboundedStack;

    while (true) {
        const auto opcode = memory[pointer];
        if (opcode == 0) { // halt: 0
            return;
        } else if (opcode == 1) { // set: 1 a b
            auto a = memory[++pointer];
            auto b = getValue(registers, memory[++pointer]);
            memory[a] = b;
        } else if (opcode == 2) { // push: 2 a
            auto a = getValue(registers, memory[++pointer]);
            unboundedStack.push(a);
        } else if (opcode == 3) { // pop: 3 a
            if (unboundedStack.empty()) {
                cerr << "Stack empty" << endl;
                return;
            }
            auto a = memory[++pointer];
            memory[a] = unboundedStack.top();
            unboundedStack.pop();
        } else if (opcode == 4) { // eq: 4 a b c
            auto a = memory[++pointer];
            auto b = getValue(registers, memory[++pointer]);
            auto c = getValue(registers, memory[++pointer]);
            memory[a] = (uint16_t) (b == c ? 1 : 0);
        } else if (opcode == 5) { // gt: 5 a b c
            auto a = memory[++pointer];
            auto b = getValue(registers, memory[++pointer]);
            auto c = getValue(registers, memory[++pointer]);
            memory[a] = (uint16_t) (b > c ? 1 : 0);
        } else if (opcode == 6) { // jmp: 6 a
            auto a = getValue(registers, memory[++pointer]);
            pointer = a;
            continue;
        } else if (opcode == 7) { // jt: 7 a b
            auto a = getValue(registers, memory[++pointer]);
            auto b = getValue(registers, memory[++pointer]);
            if (a != 0) {
                pointer = b;
                continue;
            }
        } else if (opcode == 8) { // jf: 8 a b
            auto a = getValue(registers, memory[++pointer]);
            auto b = getValue(registers, memory[++pointer]);
            if (a == 0) {
                pointer = b;
                continue;
            }
        } else if (opcode == 9) { // add: 9 a b c
            auto a = memory[++pointer];
            auto b = getValue(registers, memory[++pointer]);
            auto c = getValue(registers, memory[++pointer]);
            memory[a] = (b + c) % MAX_VALUE;
        } else if (opcode == 10) { // mult: 10 a b c
            auto a = memory[++pointer];
            auto b = getValue(registers, memory[++pointer]);
            auto c = getValue(registers, memory[++pointer]);
            memory[a] = (b * c) % MAX_VALUE;
        } else if (opcode == 11) { // mod: 11 a b c
            auto a = memory[++pointer];
            auto b = getValue(registers, memory[++pointer]);
            auto c = getValue(registers, memory[++pointer]);
            memory[a] = b % c;
        } else if (opcode == 12) { // and: 12 a b c
            auto a = memory[++pointer];
            auto b = getValue(registers, memory[++pointer]);
            auto c = getValue(registers, memory[++pointer]);
            memory[a] = b & c;
        } else if (opcode == 13) { // or: 13 a b c
            auto a = memory[++pointer];
            auto b = getValue(registers, memory[++pointer]);
            auto c = getValue(registers, memory[++pointer]);
            memory[a] = b | c;
        } else if (opcode == 14) { // not: 14 a b
            auto a = memory[++pointer];
            auto b = getValue(registers, memory[++pointer]);
            uint16_t high = (uint16_t) (b & 0x8000);
            uint16_t low = (uint16_t) (b & 0x7fff);
            memory[a] = (uint16_t) (high | (~low & 0x7fff));
        } else if (opcode == 15) { // rmem: 15 a b
            auto a = memory[++pointer];
            auto b = getValue(registers, memory[++pointer]);
            memory[a] = memory[b];
        } else if (opcode == 16) { // wmem: 16 a b
            auto a = getValue(registers, memory[++pointer]);
            auto b = getValue(registers, memory[++pointer]);
            memory[a] = b;
        } else if (opcode == 17) { // call: 17 a
            auto a = getValue(registers, memory[++pointer]);
            unboundedStack.push((uint16_t) ++pointer);
            pointer = a;
            continue;
        } else if (opcode == 18) { // ret: 18
            if (unboundedStack.empty()) {
                return;
            }
            pointer = unboundedStack.top();
            unboundedStack.pop();
            continue;
        } else if (opcode == 19) { // out: 19 a
            auto a = getValue(registers, memory[++pointer]);
            cout << (char) a;
        } else if (opcode == 20) { // in: 20 a
            if (inputBuffer.length() == 0) {
                getline(cin, inputBuffer);
                inputBuffer += '\n';
            }
            auto a = memory[++pointer];
            memory[a] = (uint16_t) inputBuffer[0];
            inputBuffer = inputBuffer.substr(1);
        } else if (opcode == 21) { // noop: 21
            // do nothing
        } else {
            cerr << "Unhandled opcode (" << opcode << ") at address " << pointer << endl;
            return;
        }
        pointer += 1;
    }
}

int main(void) {
    auto memory = initializeMemoryFromFile("/home/tim/Descargas/challenge.bin");
    auto registers = &memory[MAX_ADDRESS];

    execute(memory.get(), registers);

    return 0;
}